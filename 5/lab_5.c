#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>

#define FIFO "/tmp/fifo"
#define BUFFSIZE 64


void runserver(){
    int rd, wr;
    char buf[BUFFSIZE];
    int a, b;

    rd = open(FIFO, O_RDONLY);
    wr = open(FIFO, O_WRONLY);

    while (1){
        read(rd, buf, BUFFSIZE);
        puts(buf);
        sscanf(buf, "%d %d", &a, &b);
        sprintf(buf, "%d\n", a+b);
        write(wr, buf, BUFFSIZE);
        sleep(1);
    }

    close(rd);
    close(wr);
}

void runclient(){
    int rd, wr;
    char buf[BUFFSIZE];
    mkFIFO(FIFO, 0666);
    wr = open(FIFO, O_WRONLY);
    rd = open(FIFO, O_RDONLY);
    
    int a, b;
    while (1){
        scanf("%d %d", &a, &b);
        sprintf(buf, "%d %d\n", a, b);
        write(wr, buf, BUFFSIZE);
        sleep(1);
        read(rd, buf, BUFFSIZE);
        sscanf(buf, "%d", &a);
        printf("%d\n", a);
    }

    close(wr);
    close(rd);
}

int main(int argc, char *argv[]){
    char buf[256];
    int len = strlen(argv[0]);
    int start_pos = 0;

    for (int i = 0; i < len; ++i){
        if (argv[0][i] == '/'){
            start_pos = i + 1;
        }
    }

    for (int i = start_pos; i < len; ++i){
        buf[i - start_pos] = argv[0][i];
        buf[i - start_pos + 1] = (char)0;
    }

    if (!strcmp("server", buf)){
        printf("Starting server\n");
        runserver();
    }
    else{
        printf("Starting client\n");
        runclient();
    }

    return 0;
}
