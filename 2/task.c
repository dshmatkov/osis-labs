// lab 2 task 3
#include <stdio.h>
#include <string.h>

#define maxLength 1000

char buffer[maxLength + 10];

void convert(char *filename){
    puts(filename);
    FILE *file = fopen(filename, "r+");

    while (1) {
        memset(buffer, 0, maxLength);
        fgets(buffer, maxLength, file);
        int len = strlen(buffer);

        if (len == 0) break;

        for (int i = 0; i < len / 2 - 1; ++i){
            char temp = buffer[i];
            buffer[i] = buffer[len - i - 2];
            buffer[len - i - 2] = temp;
        }
        fseek(file, -len, SEEK_CUR);
        fputs(buffer, file);
    }
}

int main(int argc, char *argv[]){

    if (argc == 2){
        convert(argv[1]);
    }

    return 0;
}
