#include "convert.h"

char buffer[maxLength + 10];

void convert(char *filename) {
    puts(filename);
    FILE *file = fopen(filename, "r+");

    while (1) {
        memset(buffer, 0, maxLength);
        fgets(buffer, maxLength, file);
        int len = strlen(buffer);

        if (len == 0) break;

        int k = 0;
        if (buffer[len - 1] == '\n'){
            k = 1;
        }

        for (int i = 0; i < len / 2 - k; ++i) {
            char temp = buffer[i];
            buffer[i] = buffer[len - i - 1 - k];
            buffer[len - i - 1 - k] = temp;
        }
        fseek(file, -len, SEEK_CUR);
        fputs(buffer, file);
    }
}