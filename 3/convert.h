#ifndef OSIS_CONVERT_H
#define OSIS_CONVERT_H

#include <stdio.h>
#include <string.h>

#define maxLength 1000

void convert(char *filename);

#endif //OSIS_CONVERT_H
