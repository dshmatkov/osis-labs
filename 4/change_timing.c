#include <stdio.h>
#include <string.h>
#include <signal.h>

#define MULTIPLIER 5

const char *config = "config.txt";

void handler(int signal_number){
	FILE *file = fopen(config, "r+");
	char str[100];
	fgets(str, 90, file);
	fclose(file);
	
	int len = strlen(str);

	int number = 0;
	sscanf(str, "%d", &number);
	
	if (signal_number == SIGUSR1){
		number *= MULTIPLIER;
	}
	else{
		number /= MULTIPLIER;
	}	
	
	if (!number){
		number = 100;
	}
	
	file = fopen(config, "w");
	fprintf(file, "%d", number);
	fclose(file);
	
	printf("Updated");
}

int main(){
	
	int pid = fork();
	
	if (pid == -1) return -1;
	if (pid) {
		printf("%d\n\n\n", pid);
		return 0;
	}
	
	signal(SIGUSR1, handler);
	signal(SIGUSR2, handler);
	
	while (1){
		int i = 0;
		i++;
	}
	
	return 0;
}
