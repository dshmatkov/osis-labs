#include <stdio.h>
#include <unistd.h>
#include <string.h>

#define N 100

const char *config = "config.txt";

int read_time(){
	FILE *file = fopen(config, "r");
	char str[100];
	fgets(str, 90, file);
	fclose(file);
	
	int number = 0;
	sscanf(str, "%d", &number);
	return number;
}

int main(){
	
	int generation = -1;
	
	while (generation < N){
		generation++;
		
		int pid = 0;
		pid = fork();
		
		if (pid == -1) return -1;
		if (pid == 0){
			int time = read_time();
			usleep(time * 1000);
		}
		else {
			int process_id = getpid();
			printf("pid: %d; gen: %d\n", process_id, generation);
			return 0;
		}
	}
	
	return 0;
}
